package assignment.com.raghu.androdiassignment;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import assignment.com.raghu.androdiassignment.listeners.OnLoginValidateListener;
import assignment.com.raghu.androdiassignment.model.LoginModel;
import assignment.com.raghu.androdiassignment.presenters.LoginPresenter;
import assignment.com.raghu.androdiassignment.presenters.LoginPresenterContract;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by raghu on 30/7/17.
 */

public class LoginPresenterTest {


    @Mock
    LoginPresenterContract.View view;

    @Mock
    LoginModel loginModel;

    @Captor
    private ArgumentCaptor<OnLoginValidateListener> onLoginValidateListenerArgumentCaptor;

    private LoginPresenter presenter;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        presenter = new LoginPresenter(loginModel);
        presenter.onViewCreated(view);
    }

    @Test
    public void Success() {

        presenter.validate("9986929644","Raghu@123");

        verify(view).showProgress(true);
        verify(loginModel).validateCredentials(eq("9986929644"),eq("Raghu@123"),onLoginValidateListenerArgumentCaptor.capture());

        onLoginValidateListenerArgumentCaptor.getValue().onLoginSuccess();

        verify(view).showProgress(false);
        verify(view).loginSuccess();


    }
}
